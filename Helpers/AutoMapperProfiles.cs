﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DTOs;
using WebApplication1.Models;

namespace WebApplication1.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<CityData, CityDto>().ReverseMap();

            CreateMap<CityData, CityUpdateDto>().ReverseMap();

            CreateMap<CityData, CountryUpdateDto>().ReverseMap();
        }
    }
}
