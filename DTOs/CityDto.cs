﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.DTOs
{
    public class CityDto
    {
        public int Id { get; set; }

        [Required (ErrorMessage = "Name is a mandatory Field")]
        [StringLength(50 , MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        public string Country { get; set; }
    }
}
