﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Data.Repo
{
    public class CityRepository : ICityRepository
    {
        private readonly DataContext dc;

        public CityRepository(DataContext dc)
        {
            this.dc = dc;
        }

        public void AddCity(CityData city)
        {
            dc.CitiesData.Add(city);
        }

        public void DeleteCity(int id)
        {
            var city = dc.CitiesData.Find(id);
            dc.CitiesData.Remove(city);
        }

        public async Task<CityData> FindCity(int id)
        {
            return await dc.CitiesData.FindAsync(id);
        }

        public async Task<IEnumerable<CityData>> GetCitiesAsync()
        {
            return await dc.CitiesData.ToListAsync();
        }

       
    }
}
