﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Interfaces
{
    public interface ICityRepository
    {
        Task<IEnumerable<CityData>> GetCitiesAsync();
        void AddCity(CityData city);
        void DeleteCity(int id);

        Task<CityData> FindCity(int id);

    }
}
