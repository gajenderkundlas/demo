﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.DTOs;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly IUnitOfWork uow;
        private readonly IMapper mapper;

        public CityController(IUnitOfWork uow , IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetCities()
        {
            //throw new UnauthorizedAccessException();
            var cities = await uow.CityRepository.GetCitiesAsync();
            var citiesDto = mapper.Map<IEnumerable<CityDto>>(cities);
            return Ok(citiesDto);
        }



        //Post api/city/add/post  For adding Data in database in case of form submission
        [HttpPost("post")]
        public async Task<IActionResult> AddCity(CityDto cityDto)
        {
            var city = mapper.Map<CityData>(cityDto);
            city.LastUpdatedBy = 1;
            city.LastUpdatedOn = DateTime.Now;
            uow.CityRepository.AddCity(city);
            await uow.SaveAsync();
            return StatusCode(201);
        }


        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpadateCity(int id ,CityDto cityDto)
        {
            if (id != cityDto.Id) 
            {
                return BadRequest("Update Not Allowed");
            }
            var cityFromDb = await uow.CityRepository.FindCity(id);

            if (cityFromDb == null) return BadRequest("Update Not Allowed");

            cityFromDb.LastUpdatedBy = 1;
            cityFromDb.LastUpdatedOn = DateTime.Now;
            mapper.Map(cityDto , cityFromDb);
            await uow.SaveAsync();
            return StatusCode(200);
        }

        //Update only City Name
        [HttpPut("updateCityName/{id}")]
        public async Task<IActionResult> UpadateCityName(int id, CityUpdateDto cityDto)
        {
            var cityFromDb = await uow.CityRepository.FindCity(id);
            cityFromDb.LastUpdatedBy = 1;
            cityFromDb.LastUpdatedOn = DateTime.Now;
            mapper.Map(cityDto, cityFromDb);
            await uow.SaveAsync();
            return StatusCode(200);
        }

         //Update only Country Name
        [HttpPut("updateCountryName/{id}")]
        public async Task<IActionResult> UpadateCountryName(int id, CountryUpdateDto countryDto)
        {
            var cityFromDb = await uow.CityRepository.FindCity(id);
            cityFromDb.LastUpdatedBy = 1;
            cityFromDb.LastUpdatedOn = DateTime.Now;
            mapper.Map(countryDto, cityFromDb);
            await uow.SaveAsync();
            return StatusCode(200);
        }


        //Post api/city/add/city/add?cityname=New Delhi
        // [HttpPost("{add}")]
        //Post api/city/add/city/add/New Delhi To submit the value
        //[HttpPost("add/{cityName}")]
        /* public async Task<IActionResult> AddCity(string cityName)
         {
             CityData city = new CityData();
             city.Name = cityName;
             await dc.CitiesData.AddAsync(city);
             await dc.SaveChangesAsync();
             return Ok(city);
         }*/


        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteCity(int id)
        {
            uow.CityRepository.DeleteCity(id);
            await uow.SaveAsync();
            return Ok(id);

        }
    }
}
