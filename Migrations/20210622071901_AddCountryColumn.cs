﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class AddCountryColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "id",
                table: "CitiesData",
                newName: "Id");

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "CitiesData",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "CitiesData");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "CitiesData",
                newName: "id");
        }
    }
}
